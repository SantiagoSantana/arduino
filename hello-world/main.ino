/**
 * Hello World!
 * 
 * Trabajo Práctico 3
 * Ejercicio 3
 * 
 * Prender la luz de un led a intervalos regulares sin usar
 * la función `delay`.
 * 
 * La idea principal es simple, llevar registro del tiempo 
 * que pasó desde la última vez que cambió el estado del led.
 * 
 * Si ese tiempo supera un cierto intervalo, entonces el estado
 * del led cambia.
 * 
 * Para hacerlo más entretenido use los 3 leds de colores.
 * El intervalo inicial es de 500ms.
 * - Cada 500 ms pone en HIGH un led
 * - Cada 500 ms pone en LOW un led
 * 
 * La secuencia comienza:
 * 0.5s  HIGH  led verde
 * 1.0s  LOW   led amarillo
 * 1.5s  HIGH  led rojo
 * 2.0s  LOW   led verde
 * 2.5s  HIGH  led amarillo
 * 3.0s  LOW   led rojo
 * 3.5s  HIGH  led verde
 * 
 * @author: Santana Santiago
 * @date: 22/10/2018
 */
#include <Streaming.h>

const int green_led     = 25;
const int yellow_led    = 27;
const int red_led       = 29;

const int button1       = 33;
const int button2       = 31;
const int button3       = 35;

unsigned long curr;
unsigned long prev = 0;     // inicio contador del tiempo en 0

long interval = 500;        // intervalo de tiempo entre cambios
                            // de estado del led
int led_value = LOW;
int led = red_led;

boolean flag_read = false;

/**
 * Retorna el siguiente valor de la secuencia
 * ...verde, amarillo, rojo, verde, amarillo, rojo...
 */
int next_led(int led) {
  switch(led) {
    case green_led: return yellow_led;
    case yellow_led: return red_led;
    case red_led: return green_led;
  }
}


/**
 * Cambia el estado del led:
 * - Si estado es HIGH retorna LOW
 * - Si estado es LOW retorna HIGH
 */
int next_val(int led_value) {
  return (led_value == LOW) ? HIGH : LOW;
}


void setup() {
  Serial.begin(9600);
  
  pinMode(green_led, OUTPUT);
  pinMode(yellow_led, OUTPUT);
  pinMode(red_led, OUTPUT);

  pinMode(button1, INPUT);
  pinMode(button2, INPUT);
  pinMode(button3, INPUT);
}

void clickButton1() {
  if(digitalRead(button1) && !flag_read) {
    Serial << endl;
    Serial << "Seleccione la nueva frecuencia: " << endl;
    Serial << "1) 100ms" << endl;
    Serial << "2) 250ms" << endl;
    Serial << "3) 500ms" << endl << endl;
    flag_read = true;
  }
}

void loop() {
    curr = millis(); // tomo tiempo actual
    clickButton1();

    if(curr - prev > interval) {
        led_value = next_val(led_value);
        led = next_led(led);

        digitalWrite(led, led_value);
        prev = millis(); // reinicio contador de tiempo
    }
}

void serialEvent() {
    char inChar;
    char t;

    while (Serial.available()) {
        if(flag_read) {
            inChar = (char) Serial.read();
            switch(inChar) {
                case '1': interval = 100; break;
                case '2': interval = 250; break;
                case '3': interval = 500; break;
            }

            Serial << "Nueva frecuencia: " << interval << endl;
            flag_read = false;
        }

        t = Serial.read();
    }
}
